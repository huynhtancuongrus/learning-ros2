#!/usr/bin/env python3
import rclpy
from rclpy.node import Node
from my_robot_interfaces.msg import HardwareStatus
    
    
class HardwareStatusPublisherNode(Node): # MODIFY NAME
    def __init__(self):
        super().__init__("hardware_status_node") # MODIFY NAME

        self.declare_parameter("port", "/dev/ttyUSB0")

        self.port = self.get_parameter("port").value

        self.get_logger().info("Hardware Status node has been started on port %r."% (self.port, ))
        self.publisher_ = self.create_publisher(HardwareStatus, "hardware_status", 10)
        self.timer_ = self.create_timer(2, self.callback_hw_publish)

    def callback_hw_publish(self):
        msg = HardwareStatus()
        msg.temperature = 45
        msg.are_motors_ready = True
        msg.debug_message = "Nothing special"
        self.publisher_.publish(msg)
    
    
def main(args=None):
    rclpy.init(args=args)
    node = HardwareStatusPublisherNode() # MODIFY NAME
    rclpy.spin(node)
    rclpy.shutdown()
    
    
if __name__ == "__main__":
    main()